import os
import pickle
from typing import List

import numpy as np


def prepare_cluster_centers(
    data_dir: str,
    processed_data_dir: str,
    generations: List[int],
    clusters_num: int = 4,
) -> None:
    for generation in generations:
        input_path = os.path.join(data_dir, f"clusters_centers_use_dg{generation}.pkl")
        with open(input_path, 'rb') as f:
            clusters_centers = pickle.load(f)
        clusters_centers = [clusters_centers[str(cluster_id)] for cluster_id in range(clusters_num)]
        clusters_centers = np.array(clusters_centers)
        output_path = os.path.join(processed_data_dir, f"generation_{generation}", "clusters_centers.npy")
        np.save(output_path, clusters_centers)


if __name__ == '__main__':
    prepare_cluster_centers(
        data_dir="/root/mlops_final_project/infra/data",
        processed_data_dir="/root/mlops_final_project/infra/processed_data",
        generations=[1, 2],
        clusters_num=4,
    )

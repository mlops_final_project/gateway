import logging
from typing import List, Optional

import flask
import numpy as np
import requests
from scipy import linalg, dot

EMBEDDER_URL = "http://65.21.179.36:6001/v1/models/universal-sentence-encoder-large:predict"
CLUSTER_CENTERS = np.load("/data/clusters_centers.npy")

logging.getLogger().setLevel(logging.INFO)
app = flask.Flask(__name__)


def get_embedding(text: str) -> Optional[np.array]:
    json_data = {"instances": [text]}
    response = requests.post(EMBEDDER_URL, json=json_data)
    if not response.ok:
        return
    embeddings = response.json()["predictions"]
    embedding = np.array(embeddings[0])
    return embedding


def get_cluster_id(embedding: np.array) -> int:
    cos_similarities = dot(CLUSTER_CENTERS, embedding.T) / linalg.norm(CLUSTER_CENTERS) / linalg.norm(
        embedding)
    cluster_id = np.argmin(cos_similarities)
    return cluster_id


def get_k_neighbours(cluster_id: int, embedding: np.array, top_k: int) -> Optional[List[str]]:
    response = requests.post(
        f"http://65.21.179.36:700{cluster_id}/get_k_neighbours",
        json={"embedding": embedding.tolist(), "top_k": top_k}
    )
    if not response.ok:
        return
    result = response.json()
    duplicates = result["duplicates"]
    return duplicates


@app.route("/get_duplicates", methods=["POST", "GET"])
def get_duplicates():
    data = flask.request.get_json(force=True)
    input_question = data["input_question"]
    top_k = data["top_k"]
    logging.info(f"Start get embedding")
    embedding = get_embedding(text=input_question)
    logging.info("End get embedding")
    if embedding is None:
        return flask.jsonify(is_ok=False, reason="Embedding is not found"), 201
    logging.info("Start get cluster_id")
    cluster_id = get_cluster_id(embedding=embedding)
    logging.info(f"End get cluster_id: {cluster_id}")
    logging.info("Start get duplicates")
    duplicates = get_k_neighbours(cluster_id=cluster_id, embedding=embedding, top_k=top_k)
    if duplicates is None:
        return flask.jsonify(is_ok=False, reason="Duplicates are not found"), 201
    logging.info("End get duplicates")
    return flask.jsonify(is_ok=True, duplicates=duplicates, cluster_id=int(cluster_id))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
